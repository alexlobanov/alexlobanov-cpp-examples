#ifndef _EMPLOYEE_H_
#define _EMPLOYEE_H_
#include"Str.h"
#include"Date.h"
#include"Ticket.h"
class Employee{
public:
	Employee();
	~Employee();
	int getID();
	Str& getName();
	Str& getSurname();
	Ticket& getCurrentWork();
	Str& getOccupation();
	Date& getHireDate();
	Date& getFireDate();
	int getSalary();
	void setName(const Str&);
	void setSurname(const Str&);
	void setCurrentWork(const Ticket&);
	void setOccupation(const Str&);
	void setSalary(int);
	void setHireDate(const Date&);
	void setFireDate(const Date&);
	void setHireDate(int,int,int);
	void setFireDate(int,int,int);
	void printInfo();
private:
	int ID;
	Str name;
	Str surname;
	Ticket currentWork;
	Str occupation;
	int salary;
	Date hireDate;
	Date fireDate;
	static int count;
};
#endif