#include "Manager.h"
using namespace std;

Manager::Manager():Employee(){

}

Manager::~Manager(){
}

list<Employee*>& Manager::getEmployeeList(){
	return employeeList;
}

bool Manager::employeeDesiredBonus(Employee& emp){
	if(emp.getCurrentWork().isClosed() && emp.getCurrentWork().isInTime()){
		return true;
	}
	else{
		return false;
	}
}

void Manager::addEmployee(Employee& emp){
	employeeList.push_back(&emp);
}

void Manager::removeEmployee(Employee& emp){
	employeeList.remove(&emp);
}