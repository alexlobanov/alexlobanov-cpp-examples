#include "Ticket.h"

Ticket::Ticket():closed(false){
}

Ticket::Ticket(const Date& _workBeginDate,const Date& _plannedWorkEndDate):
				workBeginDate(_workBeginDate),plannedWorkEndDate(_plannedWorkEndDate),closed(false){
}

Ticket::~Ticket(){
}

Str& Ticket::getName(){
	return name;
}

Date& Ticket::getWorkBeginDate(){
	return workBeginDate;
}

Date& Ticket::getPlannedWorkEndDate(){
	return plannedWorkEndDate;
}

void Ticket::setName(Str& _name){
	name=_name;
}
	
void Ticket::setWorkBeginDate(const Date& d){
	workBeginDate=d;
}

void Ticket::setPlannedWorkEndDate(const Date& d){
	plannedWorkEndDate=d;
}

void Ticket::close(){
	realWorkEndDate=Date();
	closed=true;
}

bool Ticket::isClosed(){
	return closed;
}
	
bool Ticket::isInTime(){
	if(realWorkEndDate>plannedWorkEndDate){
		return false;
	}
	else{
		return true;
	}
}
