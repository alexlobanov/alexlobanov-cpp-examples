#include "Supervisor.h"
#include<list>

Supervisor::Supervisor():Manager(){
}

Supervisor::~Supervisor(){
}

list<Manager*> Supervisor::getManagerList(){
	return managerList;
}

void Supervisor::addManager(Manager& manager){
	managerList.push_back(&manager);
}

void Supervisor::removeManager(Manager& manager){
	managerList.remove(&manager);
}

bool Supervisor::managerBonusDesired(Manager& manager){
	list<Employee*>::iterator employeeIterator;
	for(employeeIterator=manager.getEmployeeList().begin();employeeIterator!=manager.getEmployeeList().end();employeeIterator++){
		if(!manager.employeeDesiredBonus(**employeeIterator)){
			return false;
		}
	}
	return true;
}