#ifndef _MANAGER_H_
#define _MANAGER_H_
#include"Employee.h"
#include<list>
class Manager: public Employee{
public:
	Manager();
	~Manager();
	list<Employee*>& getEmployeeList();
	bool employeeDesiredBonus(Employee&);
	void addEmployee(Employee&);
	void removeEmployee(Employee&);
private:
	list<Employee*> employeeList;
};
#endif