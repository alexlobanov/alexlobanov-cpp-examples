#ifndef _SUPERVISOR_H_
#define _SUPERVISOR_H_
#include"Manager.h"
#include<list>

class Supervisor: public Manager{
public:
	Supervisor();
	~Supervisor();
	list<Manager*> getManagerList();
	void addManager(Manager&);
	void removeManager(Manager&);
	bool managerBonusDesired(Manager&);
private:
	list<Manager*> managerList;
};
#endif

