#include "Date.h"
#include<time.h>
#include<iomanip>

Date::Date(){
	tm *humanViewCurrent;
	time_t current;
	current=time(0);
	humanViewCurrent=localtime(&current);
	year=1900+humanViewCurrent->tm_year;
	month=1+humanViewCurrent->tm_mon;
	day=humanViewCurrent->tm_mday;
}

Date::Date(int D,int M,int Y){
	day=D;
	month=M;
	year=Y;
}

Date::Date(const Date& inDate){
	day=inDate.day;
	month=inDate.month;
	year=inDate.year;
}

Date::~Date(){
}

void Date::setDate(int D,int M,int Y){
	day=D;
	month=M;
	year=Y;
}

bool Date::operator>(const Date& compareDate) const{
	return year>compareDate.year&&month>compareDate.month&&day>compareDate.day;
	/*if(year>compareDate.year){
		if(month>compareDate.month){
			if(day>compareDate.day){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}*/
}

ostream& operator<<(ostream& os,Date& d){
	//2 - ����� ������������ ���� �� ������ ����� ���� ��� ����� � ���� ����� �����
	cout<<setw(2)<<d.day<<'.'<<setw(2)<<d.month<<'.'<<d.year;
	return os;
}