#ifndef _TICKET_H_
#define _TICKET_H_
#include"Date.h"
#include"Str.h"
class Ticket{
public:
	Ticket();
	Ticket(const Date& _workBeginDate,const Date& _workEndDate);
	~Ticket();
	Str& getName();
	Date& getWorkBeginDate();
	Date& getPlannedWorkEndDate();
	void setName(Str&);
	void setWorkBeginDate(const Date&);
	void setPlannedWorkEndDate(const Date&);
	void close();
	bool isClosed();
	bool isInTime();
private:
	Str name;
	bool closed;
	bool InTime;
	Date workBeginDate;
	Date plannedWorkEndDate;
	Date realWorkEndDate;
};
#endif