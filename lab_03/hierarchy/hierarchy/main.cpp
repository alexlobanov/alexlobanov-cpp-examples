#include<iostream>
#include"Employee.h"
#include"Manager.h"
#include"Supervisor.h"

int main(){
	cout<<"ID"<<"    Surname"<<"        Name"<<"   Hire date"<<"   Fire date"
		<<"            Occupation"<<"  Salary"<<endl<<endl;
	Employee emp1;
	emp1.setName("Vasya");
	emp1.setSurname("Pupkin");
	emp1.setOccupation("Mobile Apps");
	emp1.setSalary(25000);
	emp1.setFireDate(11,12,2014);
	emp1.printInfo();
	Employee emp2;
	emp2.setName("Klava");
	emp2.setSurname("Myshkina");
	emp2.setOccupation("Parallel progr.");
	emp2.setFireDate(11,12,2015);
	emp2.setSalary(35000);
	emp2.printInfo();
	Employee emp3;
	emp3.setName("Kevin");
	emp3.setSurname("Mitnick");
	emp3.setOccupation("Computer Security");
	emp3.setFireDate(11,12,2016);
	emp3.setSalary(40000);
	emp3.printInfo();
	Manager man1;
	man1.setName("Anonymous1");
	man1.setFireDate(11,12,2015);
	man1.addEmployee(emp1);
	man1.addEmployee(emp2);
	man1.setOccupation("Manager");
	man1.setSalary(15000);
	man1.printInfo();
	Manager man2;
	man2.setName("Anonymous2");
	man2.setFireDate(11,12,2015);
	man2.addEmployee(emp3);
	man2.setSalary(10000);
	man2.setOccupation("Manager");
	man2.printInfo();
	Supervisor supv1;
	supv1.setName("Kozel");
	supv1.setSurname("Tot yeshyo");
	supv1.addManager(man1);
	supv1.addManager(man2);
	supv1.setFireDate(1,12,2015);
	supv1.setSalary(25000);
	supv1.setOccupation("Supervisor");
	supv1.printInfo();
	return 0;
}