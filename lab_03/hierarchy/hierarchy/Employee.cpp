#include "Employee.h"
#include<iomanip>

int Employee::count=0;

Employee::Employee(){
	ID=++count;
	salary=0;
}

Employee::~Employee(){
}

void Employee::printInfo(){
	//10, 20 � 6 - ��� ����� ������������ ��� �����, �������,������������� � ��������.
	//������� �� ���������� ���������� ������.
	cout<<ID<<") "<<setw(10)<<surname<<"  "<<setw(10)<<name<<"  "<<hireDate<<"  "<<
		fireDate<<"  "<<setw(20)<<occupation<<"  "<<setw(6)<<salary<<endl;
}

int Employee::getID(){
	return ID;
}

Str& Employee::getName(){
	return name;
}

Str& Employee::getSurname(){
	return surname;
}

Ticket& Employee::getCurrentWork(){
	return currentWork;
}
	
Str& Employee::getOccupation(){
	return occupation;
}

int Employee::getSalary(){
	return salary;
}

Date& Employee::getHireDate(){
	return hireDate;
}

Date& Employee::getFireDate(){
	return fireDate;
}

void Employee::setName(const Str& _name){
	name=_name;
}

void Employee::setSurname(const Str& _surname){
	surname=_surname;
}

void Employee::setCurrentWork(const Ticket& inTicket){
	currentWork=inTicket;
}

void Employee::setOccupation(const Str& _occupation){
	occupation=_occupation;
}
	
void Employee::setSalary(int _sallary){
	salary=_sallary;
}

void Employee::setHireDate(const Date& _hireDate){
	hireDate=_hireDate;
}

void Employee::setFireDate(const Date& _hireDate){
	fireDate=_hireDate;
}

void Employee::setHireDate(int D,int M,int Y){
	hireDate=Date(D,M,Y);
}

void Employee::setFireDate(int D,int M,int Y){
	fireDate=Date(D,M,Y);
}