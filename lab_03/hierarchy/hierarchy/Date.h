#ifndef _DATE_H_
#define _DATE_H_
#include<iostream>
using namespace std;
class Date{
public:
	Date();
	Date(int,int,int);
	Date(const Date&);
	~Date();
	void setDate(int,int,int);
	bool operator>(const Date&) const;
	friend ostream& operator<<(ostream&,Date&);
private:
	int year;
	int month;
	int day;
};
#endif