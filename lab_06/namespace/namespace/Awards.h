#pragma once
namespace Awards{
	class cAwards{
	public:
		cAwards();
		virtual ~cAwards();
	};
	namespace Cat{
		class CatAwards: public cAwards{
		public:
			CatAwards();
			~CatAwards();

			char name[255];
		};
	}
	namespace Dog{
		class DogAwards: public cAwards{
		public:
			DogAwards();
			~DogAwards();

			char name[255];
		};
	}
}