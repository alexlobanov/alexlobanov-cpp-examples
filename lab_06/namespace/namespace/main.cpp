#include<iostream>
#include<string.h>
#include"Awards.h"
using namespace Awards;
using namespace std;

char fun(cAwards* inAwards){
	if(dynamic_cast<Dog::DogAwards*>(inAwards)){
		return 'D';
	}
	else{
		return 'C';
	}
}

int main(){
	Cat::CatAwards cat1;
	Dog::DogAwards dog1;
	const Dog::DogAwards& constRefDog1=dog1;

	(const_cast<Dog::DogAwards&>(constRefDog1)).name[0]='A';
	cout<<dog1.name<<endl;

	cout<<static_cast<char>(1)<<endl;

	cout<<fun(&dog1)<<endl;

	int a=0;
	unsigned char* changeThirdByte=reinterpret_cast<unsigned char*>(&a);
	changeThirdByte[2]=255;

	return 0;
}