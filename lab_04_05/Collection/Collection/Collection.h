#pragma once
#include"Iterator.h"
#define STEP 10

template<class T>
class Collection{
public:
	Collection(){mas=NULL;actualSize=realSize=0;}
	virtual ~Collection();
	friend class Iterator<T>;

	virtual Iterator<T> Find(const T&)=0;
	virtual void Add(const T&);
	virtual void Delete(const Iterator<T>&);
	virtual void DeleteAll();
	Iterator<T> begin();
	Iterator<T> end();
	int getSize();
protected:
	T* mas;
	int realSize;
	int actualSize;
};

template<class T> Collection<T>::~Collection(){
	if(mas!=NULL){
		delete []mas;
	}
}

template<class T> Iterator<T> Collection<T>::begin(){
	Iterator<T> result;
	result.pCollection=this;
	result.pElement=&mas[0];
	return result;
}

template<class T> Iterator<T> Collection<T>::end(){
	Iterator<T> result;
	result.pCollection=this;
	if(actualSize!=0){
		result.pElement=&mas[actualSize-1];
	}
	else{
		result.pElement=NULL;
	}
	return result;
}

template<class T> int Collection<T>::getSize(){
	return actualSize;
}

template<class T> void Collection<T>::Add(const T& b){
	int oldSize=realSize,i;

	if(actualSize==realSize){
		T* newMas=new T[realSize+=STEP];
		for(i=0;i<oldSize;i++){
			newMas[i]=mas[i];
		}
		if(mas!=NULL){
			delete []mas;
		}
		mas=newMas;
	}
	mas[actualSize++]=b;
}

template<class T> void Collection<T>::Delete(const Iterator<T>& b){
	int i;
	T* newMas;
	for(i=(b.pElement-mas);i<actualSize-1;i++){
		mas[i]=mas[i+1];
	}
	actualSize-=1;
	if(actualSize==(realSize-STEP)){
		newMas=new T[realSize=actualSize];
		for(i=0;i<actualSize;i++){
			newMas[i]=mas[i];
		}
		delete []mas;
		mas=newMas;
	}
}

template<class T> void Collection<T>::DeleteAll(){
	delete []mas;
	mas=NULL;
	realSize=actualSize=0;
}
