#pragma once
#include"Iterator.h"

template<class T>
class Stack_;

template<class T>
class Stack_iterator: public Iterator<T>{
public:
	Stack_iterator();
	~Stack_iterator(){}
	friend class Stack_<T>;

	virtual Iterator<T>& operator=(const Iterator<T>&);
};

template<class T> Stack_iterator<T>::Stack_iterator():Iterator(){
}

template<class T> Iterator<T>& Stack_iterator<T>::operator=(const Iterator<T>& eqIterator){
	pCollection=eqIterator.pCollection;
	pElement=eqIterator.pElement;
	return *this;
}