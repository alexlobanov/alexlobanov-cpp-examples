#pragma once
#include"Collection.h"
#include"Tree_iterator.h"
#include<queue>
#define STEP 10
#define NODECOUNT 2

template<class T>
class Tree: Collection<T>{
public:
	Tree();
	~Tree();
	friend class Tree_iterator<T>;

	void Add(const T&);
	void Delete(const Iterator<T>&);
	void DeleteAll();
	Iterator<T> Find(const T&);//����� � ������
	Iterator<T> FindDepth(const T&);//����� � �������
private:
	struct Node{
		T* pElement;
		Node* left;
		Node* right;
	};
	Node* root;
	int currentLevel;
	int elementCountOnCurrentLevel;
	int lastElementAddedPosition;

	void recursiveSearch(Node* root,const T& searchElement,T*& pElement);
	void deleteTree(Node* cRoot);
	void addElementInTree(Node*,T&);
};

template<class T> Tree<T>::Tree():Collection(){
	root=new Node;
	root->pElement=NULL;
	root->left=NULL;
	root->right=NULL;
	currentLevel=1;
	elementCountOnCurrentLevel=1;
}

template<class T> Tree<T>::~Tree(){
	deleteTree(root);
}

template<class T> void Tree<T>::Delete(const Iterator<T>& delIter){
	Collection<T>::Delete(delIter);
	int i;
	deleteTree(root);
	root=new Node;
	root->pElement=NULL;
	root->right=root->left=NULL;
	currentLevel=1;
	elementCountOnCurrentLevel=1;
	for(i=0;i<actualSize;i++){
		addElementInTree(root,mas[i]);
	}
}

template<class T> void Tree<T>::DeleteAll(){
	Collection<T>::DeleteAll();
	deleteTree(root);
	root=new Node;
	root->pElement=NULL;
	root->right=root->left=NULL;
	currentLevel=1;
	elementCountOnCurrentLevel=1;
}

template<class T> void Tree<T>::deleteTree(Node* cRoot){
	if(cRoot->left){
		deleteTree(cRoot->left);
	}
	if(cRoot->right){
		deleteTree(cRoot->right);
	}
	delete cRoot;
}

template<class T> void Tree<T>::Add(const T& b){
	Collection<T>::Add(b);
	int i;
	if(actualSize==realSize-STEP+1){
		currentLevel=1;
		elementCountOnCurrentLevel=1;
		deleteTree(root);
		root=new Node;
		root->pElement=NULL;
		root->left=root->right=NULL;
		for(i=0;i<actualSize;i++){
			addElementInTree(root,mas[i]);
		}
		return;
	}
	addElementInTree(root,mas[actualSize-1]);
}

template<class T> void Tree<T>::addElementInTree(Node* root,T& b){
	int *way,i,currentElementPosition;
	Node* node=root;
	if(root->pElement==NULL){
		root->pElement=const_cast<T*>(&b);
		lastElementAddedPosition=1;
		return;
	}
	if(lastElementAddedPosition==elementCountOnCurrentLevel){
		elementCountOnCurrentLevel*=NODECOUNT;
		currentElementPosition=1;
		currentLevel++;
	}
	else{
		currentElementPosition=lastElementAddedPosition+1;
	}
	way=new int[currentLevel-1];
	way[0]=currentElementPosition/NODECOUNT+currentElementPosition%NODECOUNT;//����� ������ ��� ��������� ������
	for(i=1;i<currentLevel-2;i++){
		way[i]=way[i-1]/NODECOUNT+way[i-1]%NODECOUNT;//����� ������ ��� ��������� ������
	}
	for(i=currentLevel-3;i>=0;i--){
		if(way[i]%NODECOUNT){//����� ������ ��� ��������� ������
			node=node->left;
		}
		else{
			node=node->right;
		}
	}
	if(currentElementPosition%NODECOUNT){//����� ������ ��� ��������� ������
		node=node->left=new Node;
	}
	else{
		node=node->right=new Node;
	}
	node->pElement=const_cast<T*>(&b);
	node->left=NULL;
	node->right=NULL;
	delete []way;
	lastElementAddedPosition=currentElementPosition;
}

template<class T> Iterator<T> Tree<T>::FindDepth(const T& searchElement){
	Tree_iterator<T> result;
	result.pCollection=this;
	result.pElement=NULL;
	recursiveSearch(root,searchElement,result.pElement);
	return result;
}

template<class T> void Tree<T>::recursiveSearch(Node* root,const T& searchElement,T*& pElement){
	if(root->left){
		recursiveSearch(root->left,searchElement,pElement);
	}
	if(searchElement==*(root->pElement)){
		pElement=root->pElement;
	}
	if(root->right){
		recursiveSearch(root->right,searchElement,pElement);
	}
}

template<class T> Iterator<T> Tree<T>::Find(const T& searchElement){
	Tree_iterator<T> result;
	result.pCollection=this;
	queue<Node*> orderQueue;
	Node* temp;
	if(!root->pElement){
		return result;
	}
	orderQueue.push(root);
	while (!orderQueue.empty()){
		temp=orderQueue.front();
		orderQueue.pop();
        if(searchElement==*(temp->pElement)){
			result.pElement=temp->pElement;
		}
        if (temp->left)
			orderQueue.push(temp->left);
        if (temp->right)
			orderQueue.push(temp->right);
    }
	return result;
}