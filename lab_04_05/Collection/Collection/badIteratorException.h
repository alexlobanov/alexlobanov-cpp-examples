#pragma once
#include<iostream>
#include<stdexcept>
#include"Iterator.h"
using namespace std;

class badIteratorException: public exception{
public:
	badIteratorException():exception(){
		cout<<"No member available pointed by iterator!"<<endl;
	}
	~badIteratorException(){}
};

