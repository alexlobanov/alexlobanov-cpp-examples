#pragma once
#include"Iterator.h"

template<class T>
class Tree;

template<class T>
class Tree_iterator: Iterator<T>{
public:
	Tree_iterator(){}
	~Tree_iterator(){}
	friend class Tree<T>;

	Iterator<T>& operator=(const Iterator<T>&);
};

template<class T> Iterator<T>& Tree_iterator<T>::operator=(const Iterator<T>& eqIterator){
	pCollection=eqIterator.pCollection;
	pElement=eqIterator.pElement;
	return *this;
}