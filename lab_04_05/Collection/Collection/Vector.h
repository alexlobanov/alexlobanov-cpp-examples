#pragma once
#include"Collection.h"
#include"Stack_.h"

template<class T>
class Vector: public Stack_<T>{
public:
	Vector():Stack_(){}
	~Vector(){}
	T& operator[](int index);
};

template<class T> T& Vector<T>::operator[](int index){
	T* resultPointer=mas+index;
	if(mas==NULL|| resultPointer<mas || resultPointer>mas+actualSize-1){
		throw badIteratorException();
	}
	return *(resultPointer);
}

