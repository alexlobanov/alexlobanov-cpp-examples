#pragma once
#include"badIteratorException.h"

template<class T>
class Collection;
template<class T>
class Stack_;
template<class T>
class Stack_iterator;
template<class T>
class Tree;
template<class T>
class Tree_iterator;

template<class T>
class Iterator{
public:
	Iterator(){pElement=NULL;pCollection=NULL;}
	virtual ~Iterator(){}
	friend class Collection<T>;
	friend class Stack_<T>;
	friend class Tree<T>;
	friend class Stack_iterator<T>;
	friend class Tree_iterator<T>;

	T& getElem();
	T& operator*();
	virtual bool isNext();
	virtual Iterator<T>& operator=(const Iterator<T>&);
	virtual Iterator<T>& operator++(int);
	virtual Iterator<T>& operator++();
	virtual Iterator<T>& operator--(int);
	virtual Iterator<T>& operator--();
	bool operator==(const Iterator<T>&);
	bool operator!=(const Iterator<T>&);
	virtual bool operator<(const Iterator<T>&);
	virtual bool operator>(const Iterator<T>&);
	virtual bool operator<=(const Iterator<T>&);
	virtual bool operator>=(const Iterator<T>&);
protected:
	T* pElement;
	Collection<T>* pCollection;
};

template<class T> bool Iterator<T>::isNext(){
	Stack_<T>* pStack=dynamic_cast<Stack_<T>*>(pCollection);
	if(pElement<&pStack->mas[pStack->actualSize-1]){
		return true;
	}
	return false;
}

template<class T> T& Iterator<T>::operator*(){
	if(pElement==NULL || pCollection==NULL ||
		pElement>(pCollection->mas+pCollection->actualSize) ||	 pElement<pCollection->mas){
		throw badIteratorException();
	}
	return *pElement;
}

template<class T> T& Iterator<T>::getElem(){
	return operator*();
}

template<class T> Iterator<T>& Iterator<T>::operator++(int){
	pElement++;
	return *this;
}

template<class T> Iterator<T>& Iterator<T>::operator++(){
	pElement++;
	return *this;
}

template<class T> Iterator<T>& Iterator<T>::operator--(int){
	pElement--;
	return *this;
}

template<class T> Iterator<T>& Iterator<T>::operator--(){
	pElement--;
	return *this;
}

template<class T> Iterator<T>& Iterator<T>::operator=(const Iterator<T>& eqIterator){
	pCollection=eqIterator.pCollection;
	pElement=eqIterator.pElement;
	return *this;
}

template<class T> bool Iterator<T>::operator==(const Iterator<T>& compareIterator){
	return (pCollection==compareIterator.pCollection)&&(pElement==compareIterator.pElement);
}

template<class T> bool Iterator<T>::operator!=(const Iterator<T>& compareIterator){
	return (pCollection!=compareIterator.pCollection)||(pElement!=compareIterator.pElement);
}

template<class T> bool Iterator<T>::operator>(const Iterator<T>& compareIterator){
	return (pCollection==compareIterator.pCollection)&&(pElement>compareIterator.pElement);
}

template<class T> bool Iterator<T>::operator<(const Iterator<T>& compareIterator){
	return (pCollection==compareIterator.pCollection)&&(pElement<compareIterator.pElement);
}

template<class T> bool Iterator<T>::operator>=(const Iterator<T>& compareIterator){
	return (pCollection==compareIterator.pCollection)&&(pElement>compareIterator.pElement || pElement==compareIterator.pElement);
}

template<class T> bool Iterator<T>::operator<=(const Iterator<T>& compareIterator){
	return (pCollection==compareIterator.pCollection)&&(pElement<compareIterator.pElement || pElement==compareIterator.pElement);
}