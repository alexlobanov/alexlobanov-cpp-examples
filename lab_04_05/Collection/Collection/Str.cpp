#include<iostream>
#include<string.h>
#include "Str.h"
Str::Str(){
	cstring=new char[1];
	cstring[0]=0;
	length=0;
}

Str::Str(char* inCstring){
	char *pCstring=inCstring;
	length=strlen(inCstring);
	
	if((cstring=new char[length+1]) == NULL){
		cout<<"Str::constructor: can't allocate "<<(length+1)<<"bytes"<<endl;
		return;
	}
	else{
		memcpy(cstring,inCstring,length+1);		
	}
	//cout<<cstring;
}

Str::Str(Str& inStr){
	length=inStr.length;
	cstring=new char[length+1];
	memcpy(cstring,inStr.cstring,length+1);
}

Str::~Str(){
	delete cstring;
}

int Str::insertString(const Str& inStr,uint index){
	return insertString(inStr.cstring,index);
}

int Str::insertString(char* inStr,uint index){
	if(index>length){
		cout<<"Str::insertString: index value more than string length"<<endl;
		return -2;
	}

	uint inLength=strlen(inStr);
	uint newLength=length+inLength;

	if(newLength<length || newLength<inLength){
		cout<<"Str::insertString: operation has been canceled, resulting string is too long"<<endl;
		return -1;
	}

	char *temp;
	if((temp=new char[newLength+1]) == NULL){
		cout<<"Str::insertString: can't allocate "<<(newLength+1)<<"bytes"<<endl;
		return 1;
	}
	memcpy(temp,cstring,index);
	memcpy(temp+index,inStr,inLength);
	memcpy(temp+index+inLength,cstring+index,length-index+1);
	delete cstring;
	cstring=temp;
	length=newLength;
	//cout<<cstring;
	return 0;
}

bool Str::iterSearchSubString(char* inStr,char **startPointer) const{
	char *pCstring,*pCstring2;
	pCstring=pCstring2=cstring;
	char *pInStr=inStr;
	uint inLength=strlen(inStr);
	if(inLength == 0){
		return false;
	}
	uint currentSelfLength=strlen(pCstring);
	uint i=0;
	while(*pCstring){
		if(inLength > (currentSelfLength-i)){
			return false;
		}
		if(*pCstring == *inStr){
			pCstring2=pCstring;
			pInStr=inStr;
			while((*pInStr == *pCstring2) && *pCstring2){
				pInStr++;
				pCstring2++;
			}
			if(*pInStr == 0){
				*startPointer=pCstring;
				return true;
			}
		}
		pCstring++;
		i++;
	}
	return false;
}

bool Str::searchSubString(const Str& inStr) const{
	return searchSubString(inStr.cstring);
}

bool Str::searchSubString(char* inStr) const{
	char *ch=NULL;
	return iterSearchSubString(inStr,&ch);
}

char* Str::returnPSubString(const Str& inStr) const{
	return returnPSubString(inStr.cstring);
}

char* Str::returnPSubString(char* inStr) const{
	char *startPointer=NULL;
	iterSearchSubString(inStr,&startPointer);
	return startPointer;
}

Str& Str::operator=(const Str& eqStr){
	return operator=(eqStr.cstring);
}

Str& Str::operator=(char* eqStr){
	uint eqLength=strlen(eqStr);
	delete cstring;
	cstring=new char[eqLength+1];
	memcpy(cstring,eqStr,eqLength+1);
	length=eqLength;
	return *this;
}

Str& Str::operator+=(const Str& appendStr){
	return operator+=(appendStr.cstring);
}

Str& Str::operator+=(char* appendStr){
	insertString(appendStr,length);
	return *this;
}

Str Str::operator+(const Str& appendStr) const{
	return operator+(appendStr.cstring);
}

Str Str::operator+(char* appendStr) const{
	Str temp;
	temp.insertString(cstring,0);
	temp.insertString(appendStr,length);
	return temp;
}

bool Str::operator==(const Str& inStr) const{
	return operator==(inStr.cstring);
}

bool Str::operator==(char* inStr) const{
	return !compare(inStr);
}

bool Str::operator<(const Str& inStr) const{
	return operator<(inStr.cstring);
}

bool Str::operator<(char* inStr) const{
	bool i=false;
	if(i=compare(inStr) != 1){
		return i;
	}
	else{
		return false;
	}
}

bool Str::operator>(const Str& inStr) const{
	return operator>(inStr.cstring);
}

bool Str::operator>(char* inStr) const{
	bool i=false;
	if(i=compare(inStr) != (-1)){
		return i;
	}
	else{
		return false;
	}
}

int Str::compare(char* inStr) const{
	uint i;
	uint stringLength=strlen(cstring);
	uint inStringLength=strlen(inStr);
	if(stringLength < inStringLength){
		return -1;
	}
	else if(stringLength > inStringLength){
		return 1;
	}

	for(i=0;i<stringLength;i++){
		if(cstring[i] < inStr[i]){
			return -1;
		}
		else if(cstring[i] > inStr[i]){
			return 1;
		}
	}
	return 0;
}

ostream& operator<<(ostream& s,const Str& str){
	cout<<str.cstring;
	return s;
}