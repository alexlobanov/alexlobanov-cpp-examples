#pragma once
#include<iostream>
#include"Collection.h"
#include"Stack_iterator.h"

template<class T>
class Stack_: public Collection<T>{
public:
	Stack_(){}
	Stack_(Stack_<T>&);
	~Stack_(){}
	friend class Stack_iterator<T>;

	Iterator<T> Find(const T&);
	T pop();
};

template<class T> Stack_<T>::Stack_(Stack_<T>& eqStack){
	T* mas=new T[realSize=eqStack.realSize];
	int i;
	for(i=0;i<eqStack.actualSize;i++){
		mas[i]=eqStack.mas[i];
	}
	actualSize=eqStack.actualSize;
}

template<class T> Iterator<T> Stack_<T>::Find(const T& b){
	int i;
	Stack_iterator<T> result;
	result.pElement=&mas[actualSize];
	result.pCollection=this;
	for(i=0;i<actualSize;i++){
		if(b==mas[i]){
			result.pElement=&mas[i];
			break;
		}
	}
	return result;
}

template<class T> T Stack_<T>::pop(){
	T* newMas;
	T result=mas[actualSize-1];
	int i;

	actualSize-=1;
	if(actualSize==(realSize-STEP)){
		newMas=new T[realSize=actualSize];
		for(i=0;i<actualSize;i++){
			newMas[i]=mas[i];
		}
		delete []mas;
		mas=newMas;
	}
	return result;
}
