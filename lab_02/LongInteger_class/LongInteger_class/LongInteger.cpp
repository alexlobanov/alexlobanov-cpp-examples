#include"LongInteger.h"
#include<string.h>

int LongInteger::maxNumberOfDigits=0;
int LongInteger::innerTypeBitCount=8*sizeof(uint);//��������������, ��� � ����� 8 ���
uint LongInteger::digitMp;
int LongInteger::radix=10;//��������� ������� ��������� ��� ����� � ������ ������ �� ��������� ����� 10

LongInteger::LongInteger(){	
	if(!maxNumberOfDigits){
		setStaticParameters();
	}
	if((num=new uint[1]) == NULL){
		cout<<"LongInteger::LongInteger(): can't allocate "<<sizeof(uint)<<" bytes"<<endl;
		return;
	}
	num[0]=0;
	masLength=1;
	sign='+';
}

LongInteger::LongInteger(char* inNumber){
	if(!maxNumberOfDigits){
		setStaticParameters();
	}

	if((num=new uint[1]) == NULL){
		cout<<"LongInteger::LongInteger(char*): can't allocate "<<sizeof(uint)<<" bytes"<<endl;
		return;
	}

	char resultSign='+';
	if((*inNumber)=='-'){
		resultSign='-';
		inNumber++;
	}
	else if((*inNumber)=='+'){
		inNumber++;
	}

	num[0]=0;
	masLength=1;
	int stringLength=strlen(inNumber);
	char* stringNumber=new char[stringLength+1];
	memcpy(stringNumber,inNumber,stringLength+1);
	LongInteger temp,maxp=digitMp;
	LongInteger multiplier=1;

	char* currentPosition=stringNumber+stringLength-maxNumberOfDigits;
	while(currentPosition>stringNumber){
		(*this)+=(temp=myAtoi(currentPosition))*multiplier;
		*currentPosition=0;
		currentPosition-=maxNumberOfDigits;
		multiplier*=maxp;
	}
	temp=myAtoi(currentPosition=stringNumber);
	(*this)+=temp*multiplier;
	delete []stringNumber;
	sign=resultSign;
}

LongInteger::LongInteger(uint eqNumber){
	if(!maxNumberOfDigits){
		setStaticParameters();
	}
	masLength=1;
	sign='+';
	if((num=new uint[masLength]) == NULL){
		cout<<"LongInteger::LongInteger(uint&): can't allocate "<<masLength*sizeof(uint)<<" bytes"<<endl;	
		return;
	}
	num[0]=eqNumber;
}

LongInteger::LongInteger(const LongInteger& eqNumber){
	masLength=eqNumber.masLength;
	sign=eqNumber.sign;
	if((num=new uint[masLength]) == NULL){
		cout<<"LongInteger::LongInteger(LongInteger&): can't allocate "<<masLength*sizeof(uint)<<" bytes"<<endl;	
		return;
	}
	memcpy(num,eqNumber.num,masLength*sizeof(uint));
}

LongInteger::~LongInteger(){
	if(num != NULL){
		delete []num;
	}
}

LongInteger& LongInteger::operator=(const LongInteger& eqNumber){
	uint* newNum;
	if((newNum=new uint[eqNumber.masLength]) == NULL){
		cout<<"LongInteger::operator=: can't allocate "<<eqNumber.masLength*sizeof(uint)<<" bytes"<<endl;	
		return *this;
	}
	masLength=eqNumber.masLength;
	sign=eqNumber.sign;
	memcpy(newNum,eqNumber.num,eqNumber.masLength*sizeof(uint));
	delete []num;
	num=newNum;
	return *this;
}

LongInteger& LongInteger::operator=(char* eqNumber){
	LongInteger temp(eqNumber);
	return operator=(temp);
}

LongInteger& LongInteger::operator=(uint eqNumber){
	uint* newNum;
	if((newNum=new uint[1]) == NULL){
		cout<<"LongInteger::operator=: can't allocate "<<sizeof(uint)<<" bytes"<<endl;
		return *this;
	}
	newNum[0]=eqNumber;
	delete []num;
	num=newNum;
	masLength=1;
	sign='+';
	return *this;
}

LongInteger LongInteger::operator+(const LongInteger& appendNumber) const{
	LongInteger result=(*this);
	return result+=appendNumber;
}

LongInteger& LongInteger::operator+=(const LongInteger& appendNumber){
	if(sign=='+'&&appendNumber.sign=='-'){
		return subtractEqualPositive(appendNumber);
	}
	else if(sign=='-'&&appendNumber.sign=='+'){
		LongInteger result=appendNumber;
		result.subtractEqualPositive(*this);
		return (*this)=result;
	}
	
	return sumEqualPositive(appendNumber);
}

LongInteger& LongInteger::sumEqualPositive(const LongInteger& appendNumber){
	if(masLength<appendNumber.masLength){
		LongInteger result=appendNumber;
		return (*this)=result.sumEqualPositive(*this);
	}

	int i,theOne=0;
	uint oldnum_i;
	uint *enoughSizeNum;

	for(i=0;i<appendNumber.masLength;i++){
		oldnum_i=num[i];
		num[i]+=(appendNumber.num[i]+theOne);
		if((num[i]<oldnum_i) || (num[i]<appendNumber.num[i])){
			theOne=1;
		}
		else if(((num[i]==oldnum_i) || (num[i]==appendNumber.num[i])) && oldnum_i && appendNumber.num[i]){
			theOne=1;
		}
		else{
			theOne=0;
		}
	}
	for(;i<masLength;i++){
		oldnum_i=num[i];
		num[i]+=theOne;
		if(num[i]<oldnum_i){
			theOne=1;
		}
		else{
			theOne=0;
		}
	}

	if(theOne){
		if((enoughSizeNum=new uint[masLength+1]) == NULL){
			cout<<"LongInteger::sumEqualPositive: can't allocate "<<(masLength+1)*sizeof(uint)<<" bytes"<<endl;
			return *this;
		}
		memcpy(enoughSizeNum,num,masLength*sizeof(uint));
		delete []num;
		num=enoughSizeNum;
		num[masLength]=1;
		masLength+=1;
	}
	return *this;
}

LongInteger LongInteger::operator-(const LongInteger& deductNumber) const{
	LongInteger result=(*this);
	return result-=deductNumber;
}

LongInteger& LongInteger::operator-=(const LongInteger& deductNumber){
	if(sign=='-'&&deductNumber.sign=='+'){
		return sumEqualPositive(deductNumber);
	}
	else if(sign=='+'&&deductNumber.sign=='-'){
		return sumEqualPositive(deductNumber);
	}
	else if(sign=='-'&&deductNumber.sign=='-'){
		LongInteger result=deductNumber;
		result.sign='+';
		result.subtractEqualPositive(*this);
		return (*this)=result;
	}

	return subtractEqualPositive(deductNumber);
}

LongInteger& LongInteger::subtractEqualPositive(const LongInteger& deductNumber){
	if(comparePositive(deductNumber)==(-1)){
		LongInteger result=deductNumber;
		result.subtractEqualPositive(*this);
		result.sign='-';
		return (*this)=result;
	}
	
	int i,theOne=0;
	uint oldnum_i;
	for(i=0;i<deductNumber.masLength;i++){
		oldnum_i=num[i];
		num[i]-=(deductNumber.num[i]+theOne);
		if(oldnum_i<deductNumber.num[i]){
			theOne=1;
		}
		else{
			theOne=0;
		}
	}
	for(;i<masLength;i++){
		oldnum_i=num[i];
		num[i]-=theOne;
		if(num[i]>oldnum_i){
			theOne=1;
		}
		else{
			theOne=0;
		}
	}
	this->reduce();
	return *this;
}

LongInteger LongInteger::operator+() const{
	return *this;
}

LongInteger LongInteger::operator-() const{
	LongInteger result=(*this);
	result.sign=sign=='+'?'-':'+';
	return result;
}

LongInteger LongInteger::operator*(const LongInteger& secondNumber) const{
	bool firstBigger=(this->comparePositive(secondNumber)==1);
	int lowerMasLength;
	int i,j;
	const LongInteger* lowerNumber;
	const LongInteger* biggerNumber;
	if(firstBigger){
		lowerMasLength=secondNumber.masLength;
		lowerNumber=&secondNumber;
		biggerNumber=this;
	}
	else{
		lowerMasLength=masLength;
		lowerNumber=this;
		biggerNumber=&secondNumber;
	}

	uint one=1;
	LongInteger result;
	for(i=lowerMasLength-1;i>=0;i--){
		for(j=innerTypeBitCount-1;j>=0;j--){
			if((one<<j)&(lowerNumber->num[i])){
				result.sumEqualPositive((*biggerNumber)<<(j+i*innerTypeBitCount));
			}
		}
	}
	result.sign=(sign==secondNumber.sign)?'+':'-';
	return result;
}

LongInteger& LongInteger::operator*=(const LongInteger& secondNumber){
	return *this=*this*secondNumber;
}

LongInteger LongInteger::operator/(const LongInteger& divisor) const{
	LongInteger result;
	if(masLength==1 && num[0]==0){
		return result;
	}
	if(divisor.comparePositive(*this)==1){
		return result;
	}

	if(divisor.masLength==1){
		if(divisor.num[0]==0){
			result.becomeNullWithSize(masLength);
			result=~result;
			result.sign=sign;
			cout<<"Division by zero!\nResult is the maximum with the same byte size as dividend's size."<<endl;
			return result;
		}
		else if(divisor.num[0]==1){
			result=*this;
			result.sign=sign==divisor.sign?'+':'-';
			return result;
		}
	}

	LongInteger one=1,changableDividend=(*this);
	int i,olderBitPosition,divisorOlderBitPosition;
	int temp,olderIndex=masLength-1,divisorOlderIndex=divisor.masLength-1;
	int bitShift;
	uint uintOne=1;

	for(i=innerTypeBitCount-1;i>=0;i--){
		if((uintOne<<i)&num[olderIndex]){
			olderBitPosition=i+1;
			break;
		}
	}
	for(i=innerTypeBitCount-1;i>=0;i--){
		if((uintOne<<i)&divisor.num[divisorOlderIndex]){
			divisorOlderBitPosition=i+1;
			break;
		}
	}
	bitShift=(masLength-divisor.masLength)*innerTypeBitCount+olderBitPosition-divisorOlderBitPosition;

	if(bitShift==0){
		one.sign=sign==divisor.sign?'+':'-';
		return one;
	}

	if((temp=(divisor<<bitShift).comparePositive(*this))==0){
		result=one<<bitShift;
		result.sign=sign==divisor.sign?'+':'-';
		return result;
	}
	else if(temp==1){
		bitShift-=1;
	}
	changableDividend.subtractEqualPositive(divisor<<bitShift);

	result.sumEqualPositive(one<<bitShift);
	bitShift--;
	while(bitShift>=0){
		if((temp=(divisor<<bitShift).comparePositive(changableDividend))==(-1)||!temp){
			changableDividend.subtractEqualPositive(divisor<<bitShift);
			result.sumEqualPositive(one<<bitShift);
		}
		bitShift--;
	}
	result.sign=sign==divisor.sign?'+':'-';
	return result;
}

LongInteger& LongInteger::operator/=(const LongInteger& divisor){
	return (*this)=(*this)/(divisor);
}

LongInteger LongInteger::operator%(const LongInteger& divisor) const{
	return (*this)-operator/(divisor)*divisor;
}

LongInteger& LongInteger::operator%=(const LongInteger& divisor){
	return (*this)=(*this)-operator/(divisor)*divisor;
}

LongInteger LongInteger::operator&(const LongInteger& secondNumber) const{
	int i,newMasLength,lowerMasLength;
	bool firstBigger=masLength>secondNumber.masLength;
	if(firstBigger){
		newMasLength=masLength;
		lowerMasLength=secondNumber.masLength;
	}
	else{
		newMasLength=secondNumber.masLength;
		lowerMasLength=masLength;
	}

	LongInteger result;
	result.becomeNullWithSize(newMasLength);
	for(i=0;i<lowerMasLength;i++){
		result.num[i]=num[i]&secondNumber.num[i];
	}
	for(;i<newMasLength;i++){
		result.num[i]=0;
	}
	return result;
}

LongInteger LongInteger::operator|(const LongInteger& secondNumber) const{
	int i,newMasLength,lowerMasLength;
	bool firstBigger=masLength>secondNumber.masLength;
	const LongInteger* biggerNumber;
	if(firstBigger){
		newMasLength=masLength;
		lowerMasLength=secondNumber.masLength;
		biggerNumber=this;
	}
	else{
		newMasLength=secondNumber.masLength;
		lowerMasLength=masLength;
		biggerNumber=&secondNumber;
	}

	LongInteger result;
	result.becomeNullWithSize(newMasLength);
	for(i=0;i<lowerMasLength;i++){
		result.num[i]=num[i]|secondNumber.num[i];
	}
	for(;i<newMasLength;i++){
		result.num[i]=biggerNumber->num[i];
	}
	return result;
}

LongInteger LongInteger::operator^(const LongInteger& secondNumber) const{
	int i,newMasLength,lowerMasLength;
	bool firstBigger=masLength>secondNumber.masLength;
	const LongInteger* biggerNumber;
	uint zero=0;
	if(firstBigger){
		newMasLength=masLength;
		lowerMasLength=secondNumber.masLength;
		biggerNumber=this;
	}
	else{
		newMasLength=secondNumber.masLength;
		lowerMasLength=masLength;
		biggerNumber=&secondNumber;
	}

	LongInteger result;
	result.becomeNullWithSize(newMasLength);
	for(i=0;i<lowerMasLength;i++){
		result.num[i]=num[i]^secondNumber.num[i];
	}
	for(;i<newMasLength;i++){
		result.num[i]=biggerNumber->num[i]^zero;
	}
	return result;
}

LongInteger LongInteger::operator~() const{
	LongInteger result;
	int i;
	result.becomeNullWithSize(masLength);
	for(i=0;i<masLength;i++){
		result.num[i]=~num[i];
	}
	return result;
}

LongInteger LongInteger::operator<<(int shift) const{
	if(shift<0){
		return operator>>(-shift);
	}
	if(shift==0 || (masLength==1 && num[0]==0)){
		return *this;
	}
	int olderBitPosition,newOlderBitPosition,newMasLength,i,j;
	int innerTypeShift;
	uint one=1;
	for(i=innerTypeBitCount-1;i>=0;i--){
		if((one<<i)&num[masLength-1]){
			olderBitPosition=i+1;
			break;
		}
	}

	newMasLength=masLength+(shift+olderBitPosition-1)/innerTypeBitCount;
	LongInteger result;
	result.becomeNullWithSize(newMasLength);
	newOlderBitPosition=(shift+olderBitPosition)%innerTypeBitCount;
	if(!newOlderBitPosition){
		newOlderBitPosition=innerTypeBitCount;
	}

	if((innerTypeShift=newOlderBitPosition-olderBitPosition) > 0){
		for(i=masLength-1,j=newMasLength-1;i>0;i--,j--){
			result.num[j]=num[i]<<innerTypeShift;
			result.num[j]+=num[i-1]>>(innerTypeBitCount-innerTypeShift);
		}
		result.num[j]=num[i]<<innerTypeShift;
	}
	else if(innerTypeShift==0){
		for(i=masLength-1,j=newMasLength-1;i>0;i--,j--){
			result.num[j]=num[i];
		}
		result.num[j]=num[i];
	}
	else{
		innerTypeShift=-innerTypeShift;
		for(i=masLength-1,j=newMasLength-1;i>=0;i--,j--){
			result.num[j]+=num[i]>>innerTypeShift;
			result.num[j-1]+=num[i]<<(innerTypeBitCount-innerTypeShift);
		}
	}
	return result;
}

LongInteger LongInteger::operator>>(int shift) const{
	if(shift<0){
		return operator<<(-shift);
	}
	if(shift==0 || (masLength==1 && num[0]==0)){
		return *this;
	}

	int olderBitPosition,newOlderBitPosition,newMasLength,i,j;
	int innerTypeShift;
	uint one=1;
	for(i=innerTypeBitCount-1;i>=0;i--){
		if((one<<i)&num[masLength-1]){
			olderBitPosition=i+1;
			break;
		}
	}

	newMasLength=(olderBitPosition-shift-1+masLength*innerTypeBitCount)/innerTypeBitCount;
	LongInteger result;
	if(newMasLength <= 0){
		return result;
	}

	result.becomeNullWithSize(newMasLength);
	newOlderBitPosition=(olderBitPosition-shift+masLength*innerTypeBitCount)%innerTypeBitCount;
	if(!newOlderBitPosition){
		newOlderBitPosition=innerTypeBitCount;
	}

	if((innerTypeShift=newOlderBitPosition-olderBitPosition) > 0){
		for(i=masLength-1,j=newMasLength-1;j>0;i--,j--){
			result.num[j]=num[i]<<innerTypeShift;
			result.num[j]+=num[i-1]>>(innerTypeBitCount-innerTypeShift);
		}
		result.num[j]=num[i]<<innerTypeShift;
	}
	else if(innerTypeShift==0){
		for(i=masLength-1,j=newMasLength-1;j>0;i--,j--){
			result.num[j]=num[i];
		}
		result.num[j]=num[i];
	}
	else{
		innerTypeShift=-innerTypeShift;
		for(i=masLength-1,j=newMasLength-1;j>0;i--,j--){
			result.num[j]+=num[i]>>innerTypeShift;
			result.num[j-1]+=num[i]<<(innerTypeBitCount-innerTypeShift);
		}
		result.num[j]+=num[i]>>innerTypeShift;
	}
	result.reduce();
	return result;
}

bool LongInteger::operator==(const LongInteger& compareNumber) const{
	return !compare(compareNumber);
}

bool LongInteger::operator==(uint compareNumber) const{
	LongInteger compareLongInteger;
	compareLongInteger.num[0]=compareNumber;
	return !compare(compareLongInteger);
}

bool LongInteger::operator!=(const LongInteger& compareNumber) const{
	return compare(compareNumber);
}

bool LongInteger::operator!=(uint compareNumber) const{
	LongInteger compareLongInteger;
	compareLongInteger.num[0]=compareNumber;
	return compare(compareLongInteger);
}

bool LongInteger::operator>(const LongInteger& compareNumber) const{
	return compare(compareNumber)==1;
}

bool LongInteger::operator>=(const LongInteger& compareNumber) const{
	int result=compare(compareNumber);
	return (result==1 || result==0);
}

bool LongInteger::operator<(const LongInteger& compareNumber) const{
	return compare(compareNumber)==(-1);
}

bool LongInteger::operator<=(const LongInteger& compareNumber) const{
	int result=compare(compareNumber);
	return (result==(-1) || result==0);
}

int LongInteger::getDataByteSize(){
	return sizeof(uint)*masLength;
}

int LongInteger::compare(const LongInteger& compareNumber) const{
	if(sign=='+'&&compareNumber.sign=='-'){
		return 1; 
	}
	else if(sign=='-'&&compareNumber.sign=='+'){
		return -1;
	}
	else if(sign=='-'&&compareNumber.sign=='-'){
		return -comparePositive(compareNumber);
	}

	return comparePositive(compareNumber);
}

int LongInteger::comparePositive(const LongInteger& compareNumber) const{
	if(masLength>compareNumber.masLength){
		return 1;
	}
	else if(masLength<compareNumber.masLength){
		return -1;
	}

	int i;
	for(i=masLength-1;i>=0;i--){
		if(num[i]>compareNumber.num[i]){
			return 1;
		}
		else if(num[i]<compareNumber.num[i]){
			return -1;
		}
	}
	return 0;
}

void LongInteger::setStaticParameters(){
	int i;
	uint multiplier=radix,temp;
	maxNumberOfDigits=0;
	while(((temp=multiplier*radix))/radix==multiplier){
		maxNumberOfDigits++;
		multiplier=temp;
	}
	maxNumberOfDigits++;
	digitMp=1;
	for(i=0;i<maxNumberOfDigits;i++){
		digitMp*=(uint)radix;
	}
}

void LongInteger::becomeNullWithSize(int _masLength){
	delete []num;
	int i;
	if((num=new uint[_masLength]) == NULL){
		cout<<"LongInteger::becomeNullWithSize(int): can't allocate "<<_masLength*sizeof(uint)<<" bytes"<<endl;
		return;
	}
	masLength=_masLength;
	sign='+';
	for(i=0;i<masLength;i++){
		num[i]=0;
	}
}

uint LongInteger::myAtoi(char* inNumber){
	int stringLength=strlen(inNumber),i;
	uint result=0,multiplier=1;
	int shiftNumber=11;
	//����� 11 ��� ����, �����, ��������, ��� ����������������� ������� ��������� ������� � ������ 'F',
	// �������������� ��� ����������������� ������� � ����� 'A')
	for(i=stringLength-1;i>=0;i--){
		if(radix<=10){
			if(inNumber[i]<'0' || inNumber[i]>(radix-1+'0')){
				cout<<"Bad input number!";
				return result;
			}
			else{
				result+=(inNumber[i]-'0')*multiplier;
			}
		}
		else if(inNumber[i]<'0' || (inNumber[i]>'9' && inNumber[i]<'A')
				|| (inNumber[i]>('A'+radix-shiftNumber) && inNumber[i]<'a') || inNumber[i]>('a'+radix-shiftNumber)){
				cout<<"Bad input number!";
				return result;
		}
		else{
			if(inNumber[i]>'0' && inNumber[i]<'9'){
				result+=(inNumber[i]-'0')*multiplier;
			}
			else if(inNumber[i]>='A' && inNumber[i]<='A'+radix-shiftNumber){
				result+=(inNumber[i]-'A'+shiftNumber-1)*multiplier;
			}
			else if(inNumber[i]>='a' && inNumber[i]<='a'+radix-shiftNumber){
				result+=(inNumber[i]-'a'+shiftNumber-1)*multiplier;
			}
		}	
		multiplier*=radix;
	}
	return result;
}

void LongInteger::reduce(){
	int i,reduceCount=0;
	uint *newNum;
	for(i=masLength-1;(num[i]==0)&&(i>0);i--){
		reduceCount++;
	}
	newNum=new uint[masLength-=reduceCount];
	memcpy(newNum,num,sizeof(uint)*masLength);
	delete []num;
	num=newNum;
}

ostream& operator<<(ostream& os,const LongInteger& number){
	if(number.masLength==1 && number.num[0]==0){
		if(number.sign=='-'){
			cout<<number.sign;
		}
		cout<<'0';
		return os;
	}

	LongInteger cut=number,digitMpLI=number.digitMp,temp,zero,radixDigitsLI;
	cut.sign='+';
	int i,k,radixDigitsLength;
	short specialShift=10,borderRadix=10,borderDigit=9;
	int specialNumber=0;
	for(i=number.radix;i!=1;i>>=1){
		specialNumber++;
	}
	uint* radixDigits=new uint[radixDigitsLength=(number.masLength*number.innerTypeBitCount/number.maxNumberOfDigits+1)/specialNumber+1];
	char* eachDigit=new char[number.maxNumberOfDigits];
	for(i=0;i<radixDigitsLength;i++){
		radixDigits[i]=0;
	}

	for(i=0;(temp=cut/digitMpLI)>zero;i++){
		radixDigitsLI=cut-temp*digitMpLI;
		radixDigits[i]=radixDigitsLI.num[0];
		cut=temp;
	}
	radixDigitsLI=cut-temp*digitMpLI;
	radixDigits[i]=radixDigitsLI.num[0];

	if(number.sign=='-'){
		cout<<number.sign;
	}

	for(k=0;k<number.maxNumberOfDigits;k++){
		eachDigit[k]=radixDigits[i]%number.radix;
		radixDigits[i]/=number.radix;
	}
	for(k=number.maxNumberOfDigits-1;k>=0;k--){
		if(eachDigit[k]){
			break;
		}
	}
	for(;k>=0;k--){
		if(number.radix>borderRadix && eachDigit[k]>borderDigit){
			cout<<(char)(eachDigit[k]-specialShift+'A');
		}
		else{
			cout<<(int)eachDigit[k];
		}
	}
	i--;
	for(;i>=0;i--){
		for(k=0;k<number.maxNumberOfDigits;k++){
			eachDigit[k]=radixDigits[i]%number.radix;
			radixDigits[i]/=number.radix;
		}
		for(k=number.maxNumberOfDigits-1;k>=0;k--){
			if(number.radix>borderRadix && eachDigit[k]>borderDigit){
				cout<<(char)(eachDigit[k]-specialShift+'A');
			}
			else{
				cout<<(int)eachDigit[k];
			}
		}
	}
	delete []eachDigit;
	delete []radixDigits;
	return os;
}

int LongInteger::getRadix(){
	return radix;
}

void LongInteger::setRadix(int _radix){
	if(_radix<2 || _radix>36){
		cout<<"Radix can be only the value between 2 and 36"<<endl
			<<"Current radix is "<<radix<<endl;
	}
	else{
		radix=_radix;
		setStaticParameters();
	}
}
