#ifndef _LONGINTEGER_H_
#define _LONGINTEGER_H_
#include<iostream>
using namespace std;
typedef unsigned int uint;

class LongInteger{
public:
	LongInteger();
	LongInteger(char*);
	LongInteger(uint);
	LongInteger(const LongInteger&);
	~LongInteger();

	LongInteger& operator=(const LongInteger&);
	LongInteger& operator=(char*);
	LongInteger& operator=(uint);
	LongInteger operator+(const LongInteger&) const;
	LongInteger& operator+=(const LongInteger&);
	LongInteger operator-(const LongInteger&) const;
	LongInteger& operator-=(const LongInteger&);
	LongInteger operator+() const;//�� ��� �� ����, �� ������ �� � �� ��������������
	LongInteger operator-() const;
	LongInteger operator*(const LongInteger&) const;
	LongInteger& operator*=(const LongInteger&);
	LongInteger operator/(const LongInteger&) const;
	LongInteger& operator/=(const LongInteger&);
	LongInteger operator%(const LongInteger&) const;
	LongInteger& operator%=(const LongInteger&);

	LongInteger operator&(const LongInteger&) const;
	LongInteger operator|(const LongInteger&) const;
	LongInteger operator~() const;
	LongInteger operator^(const LongInteger&) const;
	LongInteger operator<<(int shift) const;
	LongInteger operator>>(int shift) const;

	bool operator==(const LongInteger&) const;
	bool operator==(uint) const;
	bool operator!=(const LongInteger&) const;
	bool operator!=(uint) const;
	bool operator>(const LongInteger&) const;
	bool operator>=(const LongInteger&) const;
	bool operator<(const LongInteger&) const;
	bool operator<=(const LongInteger&) const;

	friend ostream& operator<<(ostream&,const LongInteger&);
	int getDataByteSize();
	static int getRadix();
	static void setRadix(int);
private:
	LongInteger& sumEqualPositive(const LongInteger&);
	LongInteger& subtractEqualPositive(const LongInteger&);
	int compare(const LongInteger&) const;
	int comparePositive(const LongInteger&) const;
	void becomeNullWithSize(int);
	static void setStaticParameters();
	uint myAtoi(char*);
	void reduce();

	uint* num;
	int masLength;
	char sign;

	static int maxNumberOfDigits;
	static int innerTypeBitCount;
	static uint digitMp;
	static int radix;
};
#endif