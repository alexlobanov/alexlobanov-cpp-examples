#include<iostream>
using namespace std;
int* getSimpleNumbers(int N,int& len){
	int i,j;
	int *tmas = new int[N],*mas=NULL;
	len=0;
	bool simpleFlag=true;
	for(i=2;i<=N;i++){
		for(j=2;j<i;j++){
			if((i%j) == 0){
				simpleFlag=false;
				break;
			}
		}
		if(simpleFlag){
			tmas[len++]=i;
		}
		simpleFlag=true;
	}
	mas=new int[len];
	for(i=0;i<len;i++){
		mas[i]=tmas[i];
	}
	delete tmas;
	return mas;
}
int main(){
	int N,len,*mas,i=0;
	mas=NULL;
	cout<<"Enter max number: ";
	cin>>N;
	mas=getSimpleNumbers(N,len);
	for(i=0;i<len;i++){
		cout<<mas[i]<<' ';
	}
	cout<<endl;
	return 0;
}