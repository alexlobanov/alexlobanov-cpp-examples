#ifndef _STR_H_
#define _STR_H_
using namespace std;
typedef unsigned int uint;
class Str{
public:
	Str();
	Str(char*);
	Str(Str&);
	~Str();

	//������� ������� ������ � �������� ����� index-�� �������
	int insertString(const Str& inStr,uint index);
	int insertString(char* inStr,uint index);

	//����� ��������� � �������� ������ � ��������� ����������
	bool searchSubString(const Str& inStr) const;
	bool searchSubString(char* inStr) const;

	//����� ��������� � �������� ������ � ��������� ��������� �� ������ ������� ������� ����������.
	//  ���� ������ �� �������, �� ������������ NULL
	char* returnPSubString(const Str& inStr) const;
	char* returnPSubString(char* inStr) const;

	Str operator+(const Str& appendStr) const;
	Str operator+(char* appendStr) const;

	Str& operator+=(const Str& appendStr);
	Str& operator+=(char* appendStr);

	Str& operator=(const Str& eqStr);
	Str& operator=(char* eqStr);

	bool operator==(const Str& inStr) const;
	bool operator==(char* inStr) const;

	bool operator<(const Str& inStr) const;
	bool operator<(char* inStr) const;

	bool operator>(const Str& inStr) const;
	bool operator>(char* inStr) const;

	//���������� ��������� << ��� ����������� ������ cout<<
	friend ostream& operator<<(ostream& s,const Str& str);
private:
	int compare(char* inStr) const;
	bool iterSearchSubString(char*,char**) const;

	char* cstring; //���������� "������"
	uint length;   //����� ���� "������" ��� ������� '\0'
};
#endif