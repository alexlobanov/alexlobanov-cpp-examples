#include<iostream>
#include<time.h>
using namespace std;
int setCurrentTime(int& Y,int& M,int& D){
	tm *hViewCurrent;
	time_t current;
	current=time(0);
	hViewCurrent=localtime(&current);
	Y=1900+hViewCurrent->tm_year;
	M=1+hViewCurrent->tm_mon;
	D=hViewCurrent->tm_mday;
	return 0;
}
int main(){
	int year,month,day;
	setCurrentTime(year,month,day);
	cout<<year<<'.'<<month<<'.'<<day<<endl;
}