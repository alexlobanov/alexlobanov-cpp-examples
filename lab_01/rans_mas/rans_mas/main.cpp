#include<iostream>
#include<time.h>
#define N 20
using namespace std;
struct Mas{
	int arr[N];
	int length;
};
int& getElem(int index,Mas& strMas){
	return strMas.arr[index];
}
void init(Mas& strMas){
	int i;
	int maxRand=N*2+1;
	int *a;
	for(i=0;i<N;i++){
		a=&getElem(i,strMas);
		*a=rand()%maxRand;
	}
}
int main(){
	Mas strMas;
	int i;
	srand(time(NULL));
	init(strMas);
	for(i=0;i<N;i++){
		cout<<getElem(i,strMas)<<' ';
	}
	cout<<endl;
	return 0;
}